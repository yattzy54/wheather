package com.medhelp.mamontino.wheather.data.db;

import com.medhelp.mamontino.wheather.data.model.CityLocal;
import com.medhelp.mamontino.wheather.data.model.WeatherLocal;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Single;

public interface RealmHelper
{
    Single<List<CityLocal>> getCityLocal();

    Completable saveCityLocal(List<CityLocal> response);

    Completable saveCityLocal(CityLocal response);

    Single<List<WeatherLocal>> getWeatherLocal();

    Completable saveWeatherLocal(List<WeatherLocal> response);
}
