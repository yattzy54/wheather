package com.medhelp.mamontino.wheather.ui.pager;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.medhelp.mamontino.wheather.ui.weather_fr.WeatherFragment;
import com.medhelp.mamontino.wheather.utils.main.AppConstants;

public class PagerAdapter extends FragmentStatePagerAdapter
{
    private int tabCount;
    private String city;

    public PagerAdapter(FragmentManager fragmentManager, String city)
    {
        super(fragmentManager);
        this.tabCount = 0;
        this.city = city;
    }

    @Override
    public Fragment getItem(int position)
    {
        switch (position)
        {
            case 0:
                return WeatherFragment.newInstance(city, AppConstants.WEATHER_DAY_MODE);
            case 1:
                return WeatherFragment.newInstance(city, AppConstants.WEATHER_WEEK_MODE);
            case 2:
                return WeatherFragment.newInstance(city, AppConstants.WEATHER_2_WEEK_MODE);
            default:
                return null;
        }
    }

    void setCount(int count)
    {
        tabCount = count;
    }

    @Override
    public int getCount()
    {
        return tabCount;
    }
}