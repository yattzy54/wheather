package com.medhelp.mamontino.wheather.ui.pager;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.medhelp.mamontino.wheather.MainApp;
import com.medhelp.mamontino.wheather.R;
import com.medhelp.mamontino.wheather.ui.base.BaseActivity;
import com.medhelp.mamontino.wheather.ui.rating_fr.RateFragment;
import com.medhelp.mamontino.wheather.utils.rx.RxEvents;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class PagerActivity extends BaseActivity implements PagerViewHelper
{
    private static final int TAB_COUNT = 3;

    @Inject
    PagerPresenterHelper<PagerViewHelper> presenter;

    @Inject
    CompositeDisposable disposables;

    private PagerAdapter adapter;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.view_pager)
    ViewPager viewPager;

    @BindView(R.id.tv_err_pager)
    TextView tvErrCon;

    @BindView(R.id.tab_layout)
    TabLayout tabLayout;

    public static Intent getStartIntent(Context context)
    {
        return new Intent(context, PagerActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pager);
        getActivityComponent().inject(this);
        setUnBinder(ButterKnife.bind(this));
        presenter.onAttach(this);

        if (!presenter.isNetworkMode())
        {
            tvErrCon.setVisibility(View.VISIBLE);
        }

        setupRxBus();
        setUp();
    }

    @Override
    protected void setUp()
    {
        setupToolbar();

        setupViewPager();
    }

    private void setupRxBus()
    {
        disposables.add(((MainApp) getApplication())
                .bus()
                .toObservable()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object ->
                {
                    if (object instanceof RxEvents.hasConnection)
                    {
                        tvErrCon.setVisibility(View.GONE);
                    } else if (object instanceof RxEvents.noConnection)
                    {
                        tvErrCon.setVisibility(View.VISIBLE);
                    }
                }));
    }

    private void setupViewPager()
    {
        adapter = new PagerAdapter(getSupportFragmentManager(), "Новосибирск");
        adapter.setCount(TAB_COUNT);
        viewPager.setAdapter(adapter);
        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.day_forecast)));
        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.short_forecast)));
        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.long_forecast)));
        viewPager.setOffscreenPageLimit(tabLayout.getTabCount());
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener()
        {
            @Override
            public void onTabSelected(TabLayout.Tab tab)
            {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab)
            {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab)
            {

            }
        });
    }

    private void setupToolbar()
    {
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null)
        {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back_white_24dp);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {
            case android.R.id.home:
                super.onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy()
    {
        presenter.onDetach();
        super.onDestroy();
    }

    @OnClick(R.id.fab_pager)
    public void fabClick()
    {
        showRateFragment();
    }

    public void showRateFragment()
    {
        RateFragment.newInstance().show(getSupportFragmentManager());
    }

}

