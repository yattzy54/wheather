package com.medhelp.mamontino.wheather.data.model;

import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Weather implements Serializable
{
    @SerializedName("main")
    private String main;

    @SerializedName("icon")
    private String icon;

    @NonNull
    public String getMain() {
        return main;
    }

    public void setMain(@NonNull String main) {
        this.main = main;
    }

    @NonNull
    public String getIcon() {
        return icon;
    }

    public void setIcon(@NonNull String icon) {
        this.icon = icon;
    }
}
