package com.medhelp.mamontino.wheather.ui.rating_fr;


import com.medhelp.mamontino.wheather.R;
import com.medhelp.mamontino.wheather.data.DataHelper;
import com.medhelp.mamontino.wheather.ui.base.BasePresenter;
import com.medhelp.mamontino.wheather.utils.rx.SchedulerProvider;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

public class RatePresenter<V extends RateViewHelper> extends BasePresenter<V> implements RatePresenterHelper<V>
{
    private boolean isRatingSecondaryActionShown = false;

    @Inject
    public RatePresenter(DataHelper dataHelper,
            SchedulerProvider schedulerProvider,
            CompositeDisposable compositeDisposable)
    {
        super(dataHelper, schedulerProvider, compositeDisposable);
    }

    @Override
    public void onRatingSubmitted(final float rating, String message)
    {
        if (rating == 0)
        {
            getMvpView().showMessage(R.string.rating_not_provided_error);
            return;
        }

        if (!isRatingSecondaryActionShown)
        {
            if (rating == 5)
            {
                getMvpView().showPlayStoreRatingView();
                getMvpView().hideSubmitButton();
                getMvpView().disableRatingStars();
            } else
            {
                getMvpView().showRatingMessageView();
            }
            isRatingSecondaryActionShown = true;
            return;
        }

        getMvpView().showLoading();

        getMvpView().hideLoading();
        getMvpView().showMessage(R.string.rating_thanks);
        getMvpView().dismissDialog();

    }

    private void sendRatingDataToServerInBackground(float rating)
    {

    }

    @Override
    public void onCancelClicked()
    {
        getMvpView().dismissDialog();
    }

    @Override
    public void onLaterClicked()
    {
        getMvpView().dismissDialog();
    }

    @Override
    public void onPlayStoreRatingClicked()
    {
        getMvpView().openPlayStoreForRating();
        sendRatingDataToServerInBackground(5);
        getMvpView().dismissDialog();
    }
}
