package com.medhelp.mamontino.wheather.ui.city_fr;


import com.medhelp.mamontino.wheather.ui.base.DialogMvpView;

public interface CityViewHelper extends DialogMvpView
{
    void dismissDialog();
}
