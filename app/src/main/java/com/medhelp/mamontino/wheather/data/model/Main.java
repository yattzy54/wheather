package com.medhelp.mamontino.wheather.data.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Main implements Serializable
{
    @SerializedName("temp")
    private double temp;

    @SerializedName("pressure")
    private double pressure;

    @SerializedName("humidity")
    private double humidity;

    public int getTemp() {
        return (int) temp;
    }

    public int getPressure() {
        return (int) pressure;
    }

    public int getHumidity() {
        return (int) humidity;
    }
}
