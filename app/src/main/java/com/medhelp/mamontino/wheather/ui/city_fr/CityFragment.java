package com.medhelp.mamontino.wheather.ui.city_fr;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.medhelp.mamontino.wheather.R;
import com.medhelp.mamontino.wheather.di.component.ActivityComponent;
import com.medhelp.mamontino.wheather.ui.base.BaseDialog;
import com.medhelp.mamontino.wheather.ui.main.MainActivity;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CityFragment extends BaseDialog implements CityViewHelper
{
    private static final String TAG = "CityFragment";

    @Inject
    CityPresenterHelper<CityViewHelper> presenter;

    @BindView(R.id.et_set_city)
    EditText message;

    @BindView(R.id.btn_set_city_save)
    Button saveButton;

    @BindView(R.id.btn_set_city_cancel)
    Button cancelButton;

    public static CityFragment newInstance()
    {
        CityFragment fragment = new CityFragment();
        Bundle bundle = new Bundle();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_city, container, false);
        ActivityComponent component = getActivityComponent();
        if (component != null)
        {
            component.inject(this);
            setUnBinder(ButterKnife.bind(this, view));
            presenter.onAttach(this);
        }
        message.setCursorVisible(true);
        message.animate();
        return view;
    }

    public void show(FragmentManager fragmentManager)
    {
        super.show(fragmentManager, TAG);
    }

    @Override
    protected void setUp(View view)
    {

    }

    @Override
    public void dismissDialog()
    {
        super.dismissDialog(TAG);
    }

    @SuppressWarnings("unused")
    @OnClick(R.id.btn_set_city_save)
    void onSaveClick()
    {
        String newCity = message.getText().toString().trim().toLowerCase();

        assert this.getActivity() != null;

        if (newCity.length() > 2)
        {
            ((MainActivity) this.getActivity()).findCity(newCity);

            dismissDialog();

        } else if (newCity.length() == 0)
        {
            ((MainActivity) this.getActivity()).showError("Необходимо ввести название города");
        } else
        {
            ((MainActivity) this.getActivity()).showError("Слишком мало данных");
        }
    }

    @SuppressWarnings("unused")
    @OnClick(R.id.btn_set_city_cancel)
    void onCancelClick()
    {
        dismissDialog();
    }
}