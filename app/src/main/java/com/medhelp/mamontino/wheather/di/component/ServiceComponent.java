package com.medhelp.mamontino.wheather.di.component;


import com.medhelp.mamontino.wheather.bg.SyncService;
import com.medhelp.mamontino.wheather.di.module.ActivityModule;
import com.medhelp.mamontino.wheather.di.module.ServiceModule;
import com.medhelp.mamontino.wheather.di.scope.PerService;

import dagger.Component;


@PerService
@Component(dependencies = AppComponent.class, modules = {ServiceModule.class, ActivityModule.class})
public interface ServiceComponent
{
    void inject(SyncService service);
}