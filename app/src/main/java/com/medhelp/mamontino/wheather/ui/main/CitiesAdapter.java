package com.medhelp.mamontino.wheather.ui.main;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.medhelp.mamontino.wheather.R;
import com.medhelp.mamontino.wheather.data.model.City;
import com.medhelp.mamontino.wheather.ui.base.BaseViewHolder;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CitiesAdapter extends RecyclerView.Adapter<BaseViewHolder>
{
    @Inject
    MainPresenter presenter;

    private static final int VIEW_TYPE_NORMAL = 11;

    private List<City> response;

    public CitiesAdapter(ArrayList<City> response)
    {
        this.response = response;
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position)
    {
        holder.onBind(position);
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        switch (viewType)
        {
            case VIEW_TYPE_NORMAL:
                return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_city, parent, false));
            default:
                return new EmptyViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_empty, parent, false));
        }
    }

    @Override
    public int getItemViewType(int position)
    {
        if (response != null && response.size() > 0)
        {
            return VIEW_TYPE_NORMAL;
        }
        return 1;
    }

    @Override
    public int getItemCount()
    {
        return response.size();
    }

    void addItems(List<City> repoList)
    {
        response.clear();
        response.addAll(repoList);
        notifyDataSetChanged();
    }

    class ViewHolder extends BaseViewHolder
    {
        @BindView(R.id.city_name)
        TextView tvTitle;

        @BindView(R.id.temperature)
        TextView tvTemp;


        ViewHolder(View itemView)
        {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        protected void clear()
        {
            tvTitle.setText("");
            tvTemp.setText("");
        }

        public void onBind(int position)
        {
            super.onBind(position);
            City repo = response.get(position);
            if (repo != null)
            {
                tvTitle.setText(repo.getName());
                if (repo.getMain() != null)
                {
                    String temp = this.tvTemp.getResources().getString(R.string.f_temperature, repo.getMain().getTemp());
                    tvTemp.setText(temp);
                }
            }
        }
    }

    class EmptyViewHolder extends BaseViewHolder
    {
        @BindView(R.id.tv_empty)
        TextView tvEmpty;

        EmptyViewHolder(View itemView)
        {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @Override
        protected void clear()
        {
        }
    }
}
