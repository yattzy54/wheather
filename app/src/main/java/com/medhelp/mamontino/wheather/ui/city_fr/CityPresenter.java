package com.medhelp.mamontino.wheather.ui.city_fr;


import com.medhelp.mamontino.wheather.data.DataHelper;
import com.medhelp.mamontino.wheather.ui.base.BasePresenter;
import com.medhelp.mamontino.wheather.utils.rx.SchedulerProvider;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

public class CityPresenter<V extends CityViewHelper> extends BasePresenter<V> implements CityPresenterHelper<V>
{
    @Inject
    public CityPresenter(DataHelper dataHelper,
            SchedulerProvider schedulerProvider,
            CompositeDisposable compositeDisposable)
    {
        super(dataHelper, schedulerProvider, compositeDisposable);
    }
}
