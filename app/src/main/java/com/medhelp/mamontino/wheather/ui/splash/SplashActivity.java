package com.medhelp.mamontino.wheather.ui.splash;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;

import com.medhelp.mamontino.wheather.R;
import com.medhelp.mamontino.wheather.bg.SyncService;
import com.medhelp.mamontino.wheather.data.model.CityLocal;
import com.medhelp.mamontino.wheather.ui.base.BaseActivity;
import com.medhelp.mamontino.wheather.ui.main.MainActivity;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SplashActivity extends BaseActivity implements SplashViewHelper
{
    @Inject
    SplashPresenterHelper<SplashViewHelper> presenter;

    @BindView(R.id.img_splash)
    ImageView imgSplash;

    private static final int DELAY = 2500;
    private Timer time;

    public static Intent getStartIntent(Context context)
    {
        return new Intent(context, SplashActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        ButterKnife.bind(this);
        getActivityComponent().inject(this);
        presenter.onAttach(SplashActivity.this);
        setUp();
    }

    @Override
    protected void setUp()
    {
        startSyncService();
    }

    @Override
    protected void onStart()
    {
        super.onStart();

        rotateImage();

        if (presenter.isFirstStart())
        {
            setFirstStartData();
        }
    }

    @Override
    protected void onPause()
    {
        super.onPause();
        time.cancel();
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        startApp();
    }

    @Override
    protected void onStop()
    {
        super.onStop();
    }

    private void rotateImage()
    {
        final RotateAnimation rotate = new RotateAnimation(0.0f, 360.0f,
                RotateAnimation.RELATIVE_TO_SELF, 0.5f, RotateAnimation.RELATIVE_TO_SELF, 0.5f);
        rotate.setDuration(1000);
        rotate.setFillAfter(true);
        imgSplash.startAnimation(rotate);
    }

    @Override
    public void openMainActivity()
    {
        Intent intent = MainActivity.getStartIntent(this);
        startActivity(intent);
        finish();
    }

    private void setFirstStartData()
    {
        List<CityLocal> firstData = new ArrayList<>();
        firstData.add(new CityLocal("Москва"));
        firstData.add(new CityLocal("Санкт-Петербург"));
        firstData.add(new CityLocal("Новосибирск"));
        presenter.saveCities(firstData);
    }

    public void startApp()
    {
        time = new Timer();

        time.schedule(new TimerTask()
        {
            @Override
            public void run()
            {
                presenter.openNextActivity();
                finish();
            }
        }, DELAY);
    }

    @Override
    protected void onDestroy()
    {
        presenter.onDetach();
        super.onDestroy();
    }

    private void startSyncService()
    {
        Intent intent = SyncService.getStartIntent(this);
        startService(intent);
    }
}
