package com.medhelp.mamontino.wheather.data.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Wind implements Serializable
{
    @SerializedName("speed")
    private double speed;

    public int getSpeed()
    {
        return (int) speed;
    }
}
