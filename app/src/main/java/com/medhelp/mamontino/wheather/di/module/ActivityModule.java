package com.medhelp.mamontino.wheather.di.module;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;

import com.medhelp.mamontino.wheather.di.scope.PerActivity;
import com.medhelp.mamontino.wheather.ui.main.CitiesAdapter;
import com.medhelp.mamontino.wheather.ui.main.MainPresenter;
import com.medhelp.mamontino.wheather.ui.main.MainPresenterHelper;
import com.medhelp.mamontino.wheather.ui.main.MainViewHelper;
import com.medhelp.mamontino.wheather.ui.rating_fr.RatePresenter;
import com.medhelp.mamontino.wheather.ui.rating_fr.RatePresenterHelper;
import com.medhelp.mamontino.wheather.ui.rating_fr.RateViewHelper;
import com.medhelp.mamontino.wheather.ui.city_fr.CityPresenter;
import com.medhelp.mamontino.wheather.ui.city_fr.CityPresenterHelper;
import com.medhelp.mamontino.wheather.ui.city_fr.CityViewHelper;
import com.medhelp.mamontino.wheather.ui.splash.SplashPresenter;
import com.medhelp.mamontino.wheather.ui.splash.SplashPresenterHelper;
import com.medhelp.mamontino.wheather.ui.splash.SplashViewHelper;
import com.medhelp.mamontino.wheather.ui.pager.PagerPresenter;
import com.medhelp.mamontino.wheather.ui.pager.PagerPresenterHelper;
import com.medhelp.mamontino.wheather.ui.pager.PagerViewHelper;
import com.medhelp.mamontino.wheather.ui.weather_fr.WeatherPresenter;
import com.medhelp.mamontino.wheather.ui.weather_fr.WeatherPresenterHelper;
import com.medhelp.mamontino.wheather.ui.weather_fr.WeatherViewHelper;
import com.medhelp.mamontino.wheather.utils.rx.AppSchedulerProvider;
import com.medhelp.mamontino.wheather.utils.rx.SchedulerProvider;

import java.util.ArrayList;

import dagger.Module;
import dagger.Provides;
import io.reactivex.disposables.CompositeDisposable;


@Module
public class ActivityModule
{
    private AppCompatActivity activity;

    public ActivityModule(AppCompatActivity activity)
    {
        this.activity = activity;
    }

    @PerActivity
    @Provides
    Context provideContext()
    {
        return activity;
    }

    @PerActivity
    @Provides
    AppCompatActivity provideActivity()
    {
        return activity;
    }

    @PerActivity
    @Provides
    CompositeDisposable provideCompositeDisposable()
    {
        return new CompositeDisposable();
    }

    @PerActivity
    @Provides
    SchedulerProvider provideSchedulerProvider()
    {
        return new AppSchedulerProvider();
    }

    @PerActivity
    @Provides
    SplashPresenterHelper<SplashViewHelper> provideSplashPresenter(SplashPresenter<SplashViewHelper> presenter)
    {
        return presenter;
    }

    @PerActivity
    @Provides
    MainPresenterHelper<MainViewHelper> provideMainPresenter(MainPresenter<MainViewHelper> presenter)
    {
        return presenter;
    }

    @Provides
    WeatherPresenterHelper<WeatherViewHelper> provideChatListPresenter(WeatherPresenter<WeatherViewHelper> presenter)
    {
        return presenter;
    }

    @Provides
    RatePresenterHelper<RateViewHelper> provideRatePresenter(RatePresenter<RateViewHelper> presenter)
    {
        return presenter;
    }

    @Provides
    CityPresenterHelper<CityViewHelper> provideCityPresenter(CityPresenter<CityViewHelper> presenter)
    {
        return presenter;
    }

    @PerActivity
    @Provides
    CitiesAdapter provideContactsAdapter()
    {
        return new CitiesAdapter(new ArrayList<>());
    }

    @PerActivity
    @Provides
    PagerPresenterHelper<PagerViewHelper> providePagerPresenter(PagerPresenter<PagerViewHelper> presenter)
    {
        return presenter;
    }

    @Provides
    LinearLayoutManager provideLinearLayoutManager(AppCompatActivity activity)
    {
        return new LinearLayoutManager(activity);
    }
}
