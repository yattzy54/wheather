package com.medhelp.mamontino.wheather.ui.splash;


import com.medhelp.mamontino.wheather.ui.base.MvpView;

public interface SplashViewHelper extends MvpView
{
    void openMainActivity();
}
