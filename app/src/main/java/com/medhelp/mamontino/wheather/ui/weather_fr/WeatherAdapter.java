package com.medhelp.mamontino.wheather.ui.weather_fr;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.medhelp.mamontino.wheather.R;
import com.medhelp.mamontino.wheather.data.model.City;
import com.medhelp.mamontino.wheather.ui.base.BaseViewHolder;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class WeatherAdapter extends RecyclerView.Adapter<BaseViewHolder>
{
    private static final int VIEW_TYPE_EMPTY = 10;
    private static final int VIEW_TYPE_FROM = 11;

    private List<City> response;

    public WeatherAdapter(List<City> response)
    {
        this.response = response;
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position)
    {
        holder.onBind(position);
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        switch (viewType)
        {
            case VIEW_TYPE_FROM:
                return new WeatherViewHolder(
                        LayoutInflater.from(parent.getContext()).inflate(R.layout.item_empty, parent, false));
            default:
                return new ErrorViewHolder(
                        LayoutInflater.from(parent.getContext()).inflate(R.layout.item_empty, parent, false));
        }
    }

    @Override
    public int getItemViewType(int position)
    {
        if (response != null && response.size() > 0)
        {
                return VIEW_TYPE_FROM;
        } else
        {
            return VIEW_TYPE_EMPTY;
        }
    }

    @Override
    public int getItemCount()
    {
        if (response != null && response.size() > 0)
        {
            return response.size();
        } else
        {
            return 1;
        }
    }

    void addItems(List<City> repoList)
    {
        response.clear();
        response.addAll(repoList);
        notifyDataSetChanged();
    }



    class WeatherViewHolder extends BaseViewHolder
    {
//        @BindView(R.id.message_from)
//        TextView messageFrom;
//
//        @BindView(R.id.timestamp_from)
//        TextView timestampFrom;

        WeatherViewHolder(View itemView)
        {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        protected void clear()
        {
//            messageFrom.setText("");
//            timestampFrom.setText("");
        }

        public void onBind(int position)
        {
            super.onBind(position);

//            final MessageResponse repo = response.get(position);
//
//            if (repo.getTimeStamp() != null)
//            {
//                timestampFrom.setText(TimesUtils.getTime(repo.getTimeStamp()));
//            }
//
//            if (repo.getMessage() != null)
//            {
//                messageFrom.setText(repo.getMessage());
//            }
        }
    }

    class ErrorViewHolder extends BaseViewHolder
    {
        @BindView(R.id.tv_empty)
        TextView tvEmpty;

        ErrorViewHolder(View itemView)
        {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @Override
        protected void clear()
        {

        }
    }
}
