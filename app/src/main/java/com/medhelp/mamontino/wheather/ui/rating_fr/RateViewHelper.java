package com.medhelp.mamontino.wheather.ui.rating_fr;


import com.medhelp.mamontino.wheather.ui.base.DialogMvpView;

public interface RateViewHelper extends DialogMvpView
{
    void openPlayStoreForRating();

    void showPlayStoreRatingView();

    void showRatingMessageView();

    void hideSubmitButton();

    void disableRatingStars();

    void dismissDialog();
}
