package com.medhelp.mamontino.wheather;

import android.app.Application;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.interceptors.HttpLoggingInterceptor;
import com.crashlytics.android.Crashlytics;
import com.medhelp.mamontino.wheather.di.component.AppComponent;
import com.medhelp.mamontino.wheather.di.component.DaggerAppComponent;
import com.medhelp.mamontino.wheather.di.module.AppModule;
import com.medhelp.mamontino.wheather.utils.rx.RxBus;

import io.fabric.sdk.android.Fabric;
import io.realm.Realm;
import timber.log.Timber;

public class MainApp extends Application
{
    private RxBus bus;

    private AppComponent appComponent;

    @Override
    public void onCreate()
    {
        super.onCreate();
        Fabric.with(this, new Crashlytics());

        bus = new RxBus();

        Realm.init(this);

        appComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .build();
        appComponent.inject(this);

        AndroidNetworking.initialize(getApplicationContext());
        if (BuildConfig.DEBUG)
        {
            Timber.plant(new Timber.DebugTree());
            AndroidNetworking.enableLogging(HttpLoggingInterceptor.Level.BODY);
        }
    }

    public RxBus bus()
    {
        return bus;
    }

    public AppComponent getComponent()
    {
        return appComponent;
    }

    public void setComponent(AppComponent appComponent)
    {
        this.appComponent = appComponent;
    }

}
