package com.medhelp.mamontino.wheather.data.model;


import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class CityLocal extends RealmObject
{
    private int id;

    @PrimaryKey
    private String name;

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }


    public CityLocal()
    {
    }

    public CityLocal(String name)
    {
        this.name = name;
    }

    public CityLocal(int id, String name)
    {
        this.id = id;
        this.name = name;
    }
}
