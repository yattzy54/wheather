package com.medhelp.mamontino.wheather.bg;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.IBinder;
import android.support.annotation.Nullable;

import com.medhelp.mamontino.wheather.MainApp;
import com.medhelp.mamontino.wheather.data.DataHelper;
import com.medhelp.mamontino.wheather.utils.main.AppConstants;
import com.medhelp.mamontino.wheather.utils.rx.RxEvents;

import javax.inject.Inject;

import timber.log.Timber;


public class SyncService extends Service
{
    private BroadcastReceiver isCon;

    @Inject
    DataHelper dataManager;

    public static Intent getStartIntent(Context context)
    {
        return new Intent(context, SyncService.class);
    }

    @Override
    public void onCreate()
    {
        super.onCreate();
        MainApp app = (MainApp) getApplication();
        app.getComponent().inject(this);
        changeConnectionFlag();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId)
    {
        return START_REDELIVER_INTENT;
    }

    @Override
    public void onDestroy()
    {
        if (isCon != null)
        {
            unregisterReceiver(isCon);
        }
        super.onDestroy();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent)
    {
        return null;
    }

    /**
     *  При изменении соостояния сетевого соединения меняет флаг NETWORK_MODE
     */
    private void changeConnectionFlag()
    {
        if (isCon == null)
        {
            isCon = new BroadcastReceiver()
            {
                @Override
                public void onReceive(Context context, Intent intent)
                {
                    if (dataManager.checkNetwork())
                    {
                        dataManager.setNetworkMode(AppConstants.NETWORK_MODE);
                        ((MainApp) getApplication())
                                .bus()
                                .send(new RxEvents.hasConnection());
                        Timber.d("NETWORK_MODE");
                    } else
                    {
                        dataManager.setNetworkMode(AppConstants.NOT_NETWORK_MODE);
                        ((MainApp) getApplication())
                                .bus()
                                .send(new RxEvents.noConnection());
                        Timber.d("NOT_NETWORK_MODE");
                    }
                }
            };

            IntentFilter filters = new IntentFilter();
            filters.addAction("android.net.conn.CONNECTIVITY_CHANGE");
            registerReceiver(isCon, filters);
        }
    }
}

