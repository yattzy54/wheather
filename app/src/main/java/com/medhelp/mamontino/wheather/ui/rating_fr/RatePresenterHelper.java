package com.medhelp.mamontino.wheather.ui.rating_fr;


import com.medhelp.mamontino.wheather.di.scope.PerActivity;
import com.medhelp.mamontino.wheather.ui.base.MvpPresenter;

@PerActivity
public interface RatePresenterHelper<V extends RateViewHelper> extends MvpPresenter<V>
{
    void onRatingSubmitted(float rating, String message);

    void onCancelClicked();

    void onLaterClicked();

    void onPlayStoreRatingClicked();
}
