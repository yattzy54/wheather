package com.medhelp.mamontino.wheather.data.pref;

import android.content.Context;
import android.content.SharedPreferences;

import com.medhelp.mamontino.wheather.di.scope.PerApplication;
import com.medhelp.mamontino.wheather.utils.main.AppConstants;

import javax.inject.Inject;

@PerApplication
public class PreferencesManager implements PreferencesHelper
{
    private final SharedPreferences preferences;

    @Inject
    public PreferencesManager(@PerApplication Context context)
    {
        String prefName = AppConstants.PREF_NAME;
        preferences = context.getSharedPreferences(prefName, Context.MODE_PRIVATE);
    }

    /**
     * Установить флаг подключения к сети
     * @param mode - принимает значения NETWORK_MODE или NOT_NETWORK_MODE
     */
    @Override
    public void setNetworkMode(int mode)
    {
        preferences.edit().putInt(AppConstants.PREF_KEY_NETWORK_MODE, mode).apply();
    }

    /**
     * Возвращает флаг подключения к сети
     * @return mode - значения NETWORK_MODE или NOT_NETWORK_MODE
     */
    @Override
    public int getNetworkMode()
    {
        return preferences.getInt(AppConstants.PREF_KEY_NETWORK_MODE, 0);
    }


    /**
     * Установить при первом запуске приложения
     * @param mode - принимает значения FIRST_START или NOT_FIRST_START
     */
    @Override
    public void setStartMode(int mode)
    {
        preferences.edit().putInt(AppConstants.PREF_KEY_START_MODE, mode).apply();
    }

    /**
     * Возвращает флаг впервые ли запущено приложение
     * @return mode - значения FIRST_START или NOT_FIRST_START
     */
    @Override
    public int getStartMode()
    {
        return preferences.getInt(AppConstants.PREF_KEY_START_MODE, 0);
    }
}
