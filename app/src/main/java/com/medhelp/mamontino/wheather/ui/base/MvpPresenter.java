package com.medhelp.mamontino.wheather.ui.base;


public interface MvpPresenter<V extends MvpView>
{
    void onAttach(V mvpView);

    void onDetach();
}
