package com.medhelp.mamontino.wheather.ui.main;

import android.arch.lifecycle.LifecycleObserver;

import com.medhelp.mamontino.wheather.data.DataHelper;
import com.medhelp.mamontino.wheather.data.model.City;
import com.medhelp.mamontino.wheather.data.model.CityLocal;
import com.medhelp.mamontino.wheather.ui.base.BasePresenter;
import com.medhelp.mamontino.wheather.utils.main.AppConstants;
import com.medhelp.mamontino.wheather.utils.rx.SchedulerProvider;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableObserver;
import timber.log.Timber;

public class MainPresenter<V extends MainViewHelper> extends BasePresenter<V> implements MainPresenterHelper<V>, LifecycleObserver
{
    @Inject
    MainPresenter(DataHelper dataHelper,
            SchedulerProvider schedulerProvider,
            CompositeDisposable compositeDisposable)
    {
        super(dataHelper, schedulerProvider, compositeDisposable);
    }

    private Disposable disposable;

    @Override
    public boolean isNetworkMode()
    {
        return getDataHelper().getNetworkMode() == AppConstants.NETWORK_MODE;
    }

    @Override
    public void saveCity(CityLocal city)
    {
        Timber.d("saveCities(CityLocal city)");
        getMvpView().showLoading();
        getCompositeDisposable().add(getDataHelper().saveCityLocal(city)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(() ->
                                Timber.d("Данные успешно сохранены в локальное хранилище")
                        , throwable ->
                        {
                            Timber.d("Данные успешно сохранены в локальное хранилище");
                            getMvpView().showError("Ошибка доступа к данным: " + throwable.getMessage());
                        }));
    }

    @Override
    public void saveCityAndUpdate(City city)
    {
        getMvpView().setData(city);
        CityLocal cityLocal = new CityLocal(city.getName());
        Timber.d("saveCityAndUpdate(City city)");
        getMvpView().showLoading();
        getCompositeDisposable().add(getDataHelper().saveCityLocal(cityLocal)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(() -> Timber.d("Данные успешно сохранены в локальное хранилище")
                        , throwable ->
                                getMvpView().showError("Ошибка доступа к данным: " + throwable.getMessage())));
    }

    @Override
    public void getCityLocal()
    {
        getCompositeDisposable().add(getDataHelper().getCityLocal()
                .subscribeOn(getSchedulerProvider().io())
                .map(cityLocals ->
                {
                    List<City> cities = new ArrayList<>();
                    for (CityLocal cityLocal : cityLocals)
                    {
                        City city = new City(cityLocal.getName());
                        cities.add(city);
                    }
                    return cities;
                })
                .observeOn(getSchedulerProvider().ui())
                .subscribe((cities -> getMvpView().setData(cities)),
                        throwable -> Timber.d("Ошибка доступа к данным: "
                                + throwable.getMessage())));
    }

    @Override
    public void getData(List<CityLocal> city)
    {
        Timber.d("getData(List<CityLocal> city");
        getMvpView().showLoading();

        for (CityLocal cityLocal : city)
        {
            disposable = getDataHelper().getCityApi(cityLocal.getName())
                    .subscribeOn(getSchedulerProvider().io())
                    .observeOn(getSchedulerProvider().ui())
                    .subscribeWith(new DisposableObserver<City>()
                    {
                        @Override
                        public void onNext(City cityNext)
                        {
                            Timber.e("Новый элемент: " + cityNext.getName());
                            getMvpView().setData(cityNext);
                        }

                        @Override
                        public void onError(Throwable e)
                        {
                            getMvpView().showError("Данные были загружены с ошибкой: " + e.getMessage());
                            Timber.e("Данные из сети загружены с ошибкой: " + e.getMessage());
                        }

                        @Override
                        public void onComplete()
                        {
                            Timber.e("onComplete");
                        }
                    });
        }
        if (!isViewAttached())
        {
            return;
        }
        getMvpView().hideLoading();

    }

    @Override
    public void findCity(String name)
    {
        Timber.d("findCity " + name);
        if (getDataHelper().getNetworkMode() == AppConstants.NETWORK_MODE)
        {
            getMvpView().showLoading();
            getCompositeDisposable().add(getDataHelper().getCityApi(name)
                    .subscribeOn(getSchedulerProvider().io())
                    .observeOn(getSchedulerProvider().ui())
                    .subscribe(response ->
                    {
                        if (!isViewAttached())
                        {
                            return;
                        }
                        getMvpView().hideLoading();
                        getMvpView().setData(response);
                    }, throwable ->
                    {
                        if (!isViewAttached())
                        {
                            return;
                        }

                        getMvpView().hideLoading();

                        if (throwable.getMessage() == null)
                        {
                            getMvpView().showError("Не удалось найти город " + name);
                        } else
                        {
                            getMvpView().showError("Данные были загружены с ошибкой: " + throwable.getMessage());
                            Timber.e(throwable.getMessage());
                        }
                    }));
        } else
        {
            getMvpView().showError("Error");
        }
    }

    @Override
    public void dispose()
    {
        disposable.dispose();
    }
}
