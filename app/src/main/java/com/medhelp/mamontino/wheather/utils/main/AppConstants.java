package com.medhelp.mamontino.wheather.utils.main;


public interface AppConstants
{
    int NETWORK_MODE = 100;
    int NOT_NETWORK_MODE = 101;

    int FIRST_START = 200;
    int NOT_FIRST_START = 201;

    int WEATHER_DAY_MODE = 10;
    int WEATHER_WEEK_MODE = 11;
    int WEATHER_2_WEEK_MODE = 12;

    String PREF_KEY_NETWORK_MODE = "com.medhelp.mamontino.wheather.PREF_KEY_NETWORK_MODE";
    String PREF_KEY_START_MODE = "com.medhelp.mamontino.wheather.PREF_KEY_START_MODE";

    String DB_NAME = "com.medhelp.mamontino_whether.db";
    String PREF_NAME = "com.medhelp.mamontino_whether_pref";
    String API_KEY = "4fb315d303098eebc0a1b8ef89117099";
    String SELECTED_TAB_STATE = "SELECTED_TAB_STATE";
    String SELECTED_TAB_CITY = "SELECTED_TAB_CITY";
}
