package com.medhelp.mamontino.wheather.ui.main;


import com.medhelp.mamontino.wheather.data.model.City;
import com.medhelp.mamontino.wheather.data.model.CityLocal;
import com.medhelp.mamontino.wheather.ui.base.MvpView;

import java.util.List;

public interface MainViewHelper extends MvpView
{
    void openNetworkSettings();

    void setData(List<City> response);

    void setData(City response);

    void setCities(List<CityLocal> response);
}
