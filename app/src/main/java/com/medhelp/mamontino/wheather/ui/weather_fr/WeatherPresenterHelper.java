package com.medhelp.mamontino.wheather.ui.weather_fr;


import com.medhelp.mamontino.wheather.di.scope.PerActivity;
import com.medhelp.mamontino.wheather.ui.base.MvpPresenter;

@PerActivity
public interface WeatherPresenterHelper<V extends WeatherViewHelper> extends MvpPresenter<V>
{
    void loadForecast(String name, int state);
}
