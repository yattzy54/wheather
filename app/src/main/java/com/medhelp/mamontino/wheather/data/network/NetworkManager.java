package com.medhelp.mamontino.wheather.data.network;


import com.medhelp.mamontino.wheather.data.model.City;
import com.medhelp.mamontino.wheather.data.pref.PreferencesManager;
import com.medhelp.mamontino.wheather.di.scope.PerApplication;
import com.medhelp.mamontino.wheather.utils.main.AppConstants;
import com.rx2androidnetworking.Rx2AndroidNetworking;

import javax.inject.Inject;

import io.reactivex.Observable;

@PerApplication
public class NetworkManager implements NetworkHelper
{
    private PreferencesManager prefManager;

    @Inject
    public NetworkManager(@PerApplication PreferencesManager preferencesManager)
    {
        this.prefManager = preferencesManager;
    }

    /**
     * Получение погоды на текущий день по названию города
     * @param name - название города
     * @return информация о погоде на текущий день
     */

    @Override
    public Observable<City> getCityApi(String name)
    {
        return Rx2AndroidNetworking.get(ApiEndPoint.CITY_INFO_URL)
                .addQueryParameter("q", name)
                .addQueryParameter("appid", AppConstants.API_KEY)
                .build()
                .getObjectObservable(City.class);
    }

    /**
     * Получение прогноза погоды по названию города и колличетву дней
     * @param name - название города
     * @param count - колличество дней
     * @return - прогноз на count дней
     */

    @Override
    public Observable<City> getForecastApi(String name, int count)
    {
        return Rx2AndroidNetworking.get(ApiEndPoint.CITY_FORECAST_URL)
                .addQueryParameter("q", name)
                .addQueryParameter("cnt", String.valueOf(count))
                .addQueryParameter("appid", AppConstants.API_KEY)
                .build()
                .getObjectObservable(City.class);
    }
}

