package com.medhelp.mamontino.wheather.ui.pager;


import com.medhelp.mamontino.wheather.di.scope.PerActivity;
import com.medhelp.mamontino.wheather.ui.base.MvpPresenter;

@PerActivity
public interface PagerPresenterHelper<V extends PagerViewHelper> extends MvpPresenter<V>
{
    boolean isNetworkMode();
}
