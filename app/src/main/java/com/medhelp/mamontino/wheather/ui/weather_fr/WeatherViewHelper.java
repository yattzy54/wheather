package com.medhelp.mamontino.wheather.ui.weather_fr;


import com.medhelp.mamontino.wheather.data.model.City;
import com.medhelp.mamontino.wheather.ui.base.MvpView;

public interface WeatherViewHelper extends MvpView
{
    void updateForecast(City response);
}
