package com.medhelp.mamontino.wheather.ui.weather_fr;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.medhelp.mamontino.wheather.R;
import com.medhelp.mamontino.wheather.data.DataHelper;
import com.medhelp.mamontino.wheather.data.model.City;
import com.medhelp.mamontino.wheather.di.component.ActivityComponent;
import com.medhelp.mamontino.wheather.ui.base.BaseFragment;
import com.medhelp.mamontino.wheather.utils.main.AppConstants;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class WeatherFragment extends BaseFragment implements WeatherViewHelper
{
    @Inject
    WeatherPresenterHelper<WeatherViewHelper> presenter;

    @Inject
    DataHelper dataHelper;

    @Inject
    LinearLayoutManager layoutManager;

    @BindView(R.id.rv_weather)
    RecyclerView recyclerView;

    private WeatherAdapter adapter;
    private int state;
    String name;

    public static WeatherFragment newInstance(String name, int state)
    {
        Bundle args = new Bundle();
        WeatherFragment fragment = new WeatherFragment();
        if (state != 0)
        {
            args.putInt(AppConstants.SELECTED_TAB_STATE, state);
            args.putString(AppConstants.SELECTED_TAB_CITY, name);
        }
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
            @Nullable Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_weather, container, false);

        ActivityComponent component = getActivityComponent();
        if (component != null)
        {
            component.inject(this);
            setUnBinder(ButterKnife.bind(this, view));
            presenter.onAttach(this);
            layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
            recyclerView.setLayoutManager(layoutManager);
            recyclerView.setItemAnimator(new DefaultItemAnimator());
            recyclerView.setAdapter(adapter);
        }
        return view;
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        state = getArguments().getInt(AppConstants.SELECTED_TAB_STATE, 0);
        name = getArguments().getString(AppConstants.SELECTED_TAB_CITY);
    }

    @Override
    public void onStart()
    {
        super.onStart();
        if (state != 0 && name != null)
        {
            if (state == AppConstants.WEATHER_DAY_MODE)
            {
                presenter.loadForecast(name,1);
            } else if (state == AppConstants.WEATHER_WEEK_MODE)
            {
                presenter.loadForecast(name, 7);
            } else if (state == AppConstants.WEATHER_2_WEEK_MODE)
            {
                presenter.loadForecast(name, 14);
            }
        }
    }

    @Override
    protected void setUp(View view)
    {

    }

    @Override
    public void onDestroyView()
    {
        presenter.onDetach();
        super.onDestroyView();
    }

    @Override
    public void updateForecast(City response)
    {
        List <City> list = new ArrayList<>();
        list.add(response);
        adapter = new WeatherAdapter(list);
        recyclerView.setAdapter(adapter);
    }
}
