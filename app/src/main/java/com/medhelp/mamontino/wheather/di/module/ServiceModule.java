package com.medhelp.mamontino.wheather.di.module;


import com.medhelp.mamontino.wheather.bg.SyncService;
import com.medhelp.mamontino.wheather.di.scope.PerService;

import dagger.Module;
import dagger.Provides;


@Module
public class ServiceModule
{
    private SyncService syncService;

    @PerService
    @Provides
    SyncService provideSyncService()
    {
        return syncService;
    }
}
