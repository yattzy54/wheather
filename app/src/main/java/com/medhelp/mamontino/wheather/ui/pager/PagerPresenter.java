package com.medhelp.mamontino.wheather.ui.pager;

import android.arch.lifecycle.LifecycleObserver;

import com.medhelp.mamontino.wheather.data.DataHelper;
import com.medhelp.mamontino.wheather.ui.base.BasePresenter;
import com.medhelp.mamontino.wheather.utils.main.AppConstants;
import com.medhelp.mamontino.wheather.utils.rx.SchedulerProvider;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

public class PagerPresenter<V extends PagerViewHelper> extends BasePresenter<V> implements PagerPresenterHelper<V>, LifecycleObserver
{
    @Inject
    public PagerPresenter(DataHelper dataHelper,
            SchedulerProvider schedulerProvider,
            CompositeDisposable compositeDisposable)
    {
        super(dataHelper, schedulerProvider, compositeDisposable);
    }

    @Override
    public boolean isNetworkMode()
    {
        return getDataHelper().getNetworkMode() == AppConstants.NETWORK_MODE;
    }
}
