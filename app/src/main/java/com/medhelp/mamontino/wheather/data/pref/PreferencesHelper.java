package com.medhelp.mamontino.wheather.data.pref;

public interface PreferencesHelper
{
    void setNetworkMode(int mode);

    int getNetworkMode();

    void setStartMode(int mode);

    int getStartMode();
}
