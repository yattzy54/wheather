package com.medhelp.mamontino.wheather.ui.main;


import com.medhelp.mamontino.wheather.data.model.City;
import com.medhelp.mamontino.wheather.data.model.CityLocal;
import com.medhelp.mamontino.wheather.di.scope.PerActivity;
import com.medhelp.mamontino.wheather.ui.base.MvpPresenter;

import java.util.List;

@PerActivity
public interface MainPresenterHelper<V extends MainViewHelper> extends MvpPresenter<V>
{
    boolean isNetworkMode();

    void saveCity(CityLocal city);

    void saveCityAndUpdate(City city);

    void getCityLocal();

    void getData(List<CityLocal> city);

    void findCity(String name);

    void dispose();
}
