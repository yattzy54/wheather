package com.medhelp.mamontino.wheather.di.component;

import android.app.Application;
import android.content.Context;

import com.medhelp.mamontino.wheather.MainApp;
import com.medhelp.mamontino.wheather.bg.SyncService;
import com.medhelp.mamontino.wheather.data.DataHelper;
import com.medhelp.mamontino.wheather.di.module.AppModule;
import com.medhelp.mamontino.wheather.di.scope.PerApplication;

import dagger.Component;


@PerApplication
@Component(modules = {AppModule.class})
public interface AppComponent
{
    void inject(MainApp app);

    void inject(SyncService service);

    @PerApplication
    Context context();

    Application application();

    DataHelper getDataManager();
}