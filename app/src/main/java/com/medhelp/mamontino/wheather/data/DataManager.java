package com.medhelp.mamontino.wheather.data;


import android.content.Context;

import com.medhelp.mamontino.wheather.data.db.RealmHelper;
import com.medhelp.mamontino.wheather.data.model.City;
import com.medhelp.mamontino.wheather.data.model.CityLocal;
import com.medhelp.mamontino.wheather.data.model.WeatherLocal;
import com.medhelp.mamontino.wheather.data.network.NetworkHelper;
import com.medhelp.mamontino.wheather.data.pref.PreferencesHelper;
import com.medhelp.mamontino.wheather.di.scope.PerApplication;
import com.medhelp.mamontino.wheather.utils.main.NetworkUtils;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;

/**
 * Класс-итерактор. Обеспечивает доступ к данным, сетевым запросам
 */
@PerApplication
public class DataManager implements DataHelper
{
    private final Context context;
    private final PreferencesHelper preferencesHelper;
    private final NetworkHelper networkHelper;
    private final RealmHelper realmHelper;

    @Inject
    DataManager(@PerApplication Context context,
            @PerApplication PreferencesHelper preferencesHelper,
            @PerApplication NetworkHelper networkHelper, @PerApplication RealmHelper realmHelper)
    {
        this.context = context;
        this.preferencesHelper = preferencesHelper;
        this.networkHelper = networkHelper;
        this.realmHelper = realmHelper;
    }

    @Override
    public void setNetworkMode(int mode)
    {
        preferencesHelper.setNetworkMode(mode);
    }

    @Override
    public int getNetworkMode()
    {
        return preferencesHelper.getNetworkMode();
    }

    @Override
    public void setStartMode(int mode)
    {
        preferencesHelper.setStartMode(mode);
    }

    @Override
    public int getStartMode()
    {
        return preferencesHelper.getStartMode();
    }

    @Override
    public Observable<City> getCityApi(String name)
    {
        return networkHelper.getCityApi(name);
    }

    @Override
    public Observable<City> getForecastApi(String name, int count)
    {
        return networkHelper.getForecastApi(name, count);
    }

    @Override
    public Single<List<CityLocal>> getCityLocal()
    {
        return realmHelper.getCityLocal();
    }

    @Override
    public Completable saveCityLocal(List<CityLocal> response)
    {
        return realmHelper.saveCityLocal(response);
    }

    @Override
    public Completable saveCityLocal(CityLocal response)
    {
        return realmHelper.saveCityLocal(response);
    }

    @Override
    public Single<List<WeatherLocal>> getWeatherLocal()
    {
        return realmHelper.getWeatherLocal();
    }

    @Override
    public Completable saveWeatherLocal(List<WeatherLocal> response)
    {
        return realmHelper.saveWeatherLocal(response);
    }

    @Override
    public boolean checkNetwork()
    {
        return NetworkUtils.isNetworkConnected(context);
    }
}
