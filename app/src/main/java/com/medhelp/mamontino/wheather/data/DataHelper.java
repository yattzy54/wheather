package com.medhelp.mamontino.wheather.data;


import com.medhelp.mamontino.wheather.data.db.RealmHelper;
import com.medhelp.mamontino.wheather.data.network.NetworkHelper;
import com.medhelp.mamontino.wheather.data.pref.PreferencesHelper;

public interface DataHelper extends PreferencesHelper, NetworkHelper, RealmHelper
{
    boolean checkNetwork();
}
