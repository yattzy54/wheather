package com.medhelp.mamontino.wheather.data.network;


interface ApiEndPoint
{
    String BASE_URL = "http://api.openweathermap.org/";

    String CITY_INFO_URL = BASE_URL + "data/2.5/weather?units=metric";

    String CITY_FORECAST_URL = BASE_URL + "data/2.5/forecast/daily?units=metric";
}
