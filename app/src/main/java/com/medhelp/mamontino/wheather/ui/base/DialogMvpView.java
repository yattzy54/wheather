package com.medhelp.mamontino.wheather.ui.base;


public interface DialogMvpView extends MvpView
{
    void dismissDialog(String tag);
}
