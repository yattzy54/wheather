package com.medhelp.mamontino.wheather.ui.splash;


import com.medhelp.mamontino.wheather.data.DataHelper;
import com.medhelp.mamontino.wheather.data.model.CityLocal;
import com.medhelp.mamontino.wheather.ui.base.BasePresenter;
import com.medhelp.mamontino.wheather.utils.main.AppConstants;
import com.medhelp.mamontino.wheather.utils.rx.SchedulerProvider;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import timber.log.Timber;

public class SplashPresenter<V extends SplashViewHelper> extends BasePresenter<V> implements SplashPresenterHelper<V>
{
    @Inject
    SplashPresenter(DataHelper dataHelper, SchedulerProvider schedulerProvider, CompositeDisposable compositeDisposable)
    {
        super(dataHelper, schedulerProvider, compositeDisposable);
    }

    Disposable disposable;

    @Override
    public boolean isFirstStart()
    {
        return getDataHelper().getStartMode() != AppConstants.NOT_FIRST_START;
    }

    @Override
    public void changeStartFlag()
    {
        getDataHelper().setStartMode(AppConstants.NOT_FIRST_START);
    }

    @Override
    public void saveCities(List<CityLocal> city)
    {
        getMvpView().showLoading();
        getCompositeDisposable().add(getDataHelper().saveCityLocal(city)
                .subscribe(() ->
                        {
                            Timber.e("Данные сохранены");
                            changeStartFlag();
                        }
                        , throwable ->
                                getMvpView().showError("Ошибка доступа к данным: " + throwable.getMessage())));
    }

    @Override
    public void openNextActivity()
    {
        if (getMvpView().isNetworkConnected())
        {
            startLongNetworkOperation();
        } else
        {
            startLongDbOperation();
        }
    }

    private void startLongDbOperation()
    {
        getMvpView().openMainActivity();
    }

    private void startLongNetworkOperation()
    {
        getMvpView().openMainActivity();
    }
}
