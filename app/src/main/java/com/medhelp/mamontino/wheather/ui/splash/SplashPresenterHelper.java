package com.medhelp.mamontino.wheather.ui.splash;


import com.medhelp.mamontino.wheather.data.model.CityLocal;
import com.medhelp.mamontino.wheather.di.scope.PerActivity;
import com.medhelp.mamontino.wheather.ui.base.MvpPresenter;

import java.util.List;

@PerActivity
public interface SplashPresenterHelper<V extends SplashViewHelper> extends MvpPresenter<V>
{
    boolean isFirstStart();

    void saveCities(List<CityLocal> city);

    void changeStartFlag();

    void openNextActivity();
}
