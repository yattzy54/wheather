package com.medhelp.mamontino.wheather.data.model;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class City implements Serializable
{
    @SerializedName("name")
    private String name;

    @SerializedName("weather")
    private List<Weather> weather;

    @SerializedName("main")
    private Main main;

    @SerializedName("wind")
    private Wind wind;

    public City()
    {
    }

    public City(@NonNull String name)
    {
        this.name = name;
    }

    @NonNull
    public String getName()
    {
        return name;
    }

    public void setName(@NonNull String name)
    {
        this.name = name;
    }

    @Nullable
    public Weather getWeather()
    {
        if (weather == null || weather.isEmpty())
        {
            return null;
        }
        return weather.get(0);
    }

    @Nullable
    public Main getMain()
    {
        return main;
    }

    @Nullable
    public Wind getWind()
    {
        return wind;
    }
}
