package com.medhelp.mamontino.wheather.ui.city_fr;


import com.medhelp.mamontino.wheather.di.scope.PerActivity;
import com.medhelp.mamontino.wheather.ui.base.MvpPresenter;

@PerActivity
public interface CityPresenterHelper<V extends CityViewHelper> extends MvpPresenter<V>
{

}
