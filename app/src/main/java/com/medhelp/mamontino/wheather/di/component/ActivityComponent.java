package com.medhelp.mamontino.wheather.di.component;


import com.medhelp.mamontino.wheather.di.module.ActivityModule;
import com.medhelp.mamontino.wheather.di.scope.PerActivity;
import com.medhelp.mamontino.wheather.ui.city_fr.CityFragment;
import com.medhelp.mamontino.wheather.ui.weather_fr.WeatherFragment;
import com.medhelp.mamontino.wheather.ui.pager.PagerActivity;
import com.medhelp.mamontino.wheather.ui.main.MainActivity;
import com.medhelp.mamontino.wheather.ui.rating_fr.RateFragment;
import com.medhelp.mamontino.wheather.ui.splash.SplashActivity;

import dagger.Component;


@PerActivity
@Component(dependencies = AppComponent.class, modules = ActivityModule.class)

public interface ActivityComponent
{
    void inject(SplashActivity activity);

    void inject(MainActivity activity);

    void inject(PagerActivity pagerActivity);

    void inject(RateFragment rateFragment);

    void inject(WeatherFragment weatherFragment);

    void inject(CityFragment cityFragment);
}
