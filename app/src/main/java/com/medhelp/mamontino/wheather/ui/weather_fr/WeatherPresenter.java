package com.medhelp.mamontino.wheather.ui.weather_fr;


import com.medhelp.mamontino.wheather.data.DataHelper;
import com.medhelp.mamontino.wheather.ui.base.BasePresenter;
import com.medhelp.mamontino.wheather.utils.main.AppConstants;
import com.medhelp.mamontino.wheather.utils.rx.SchedulerProvider;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

public class WeatherPresenter<V extends WeatherViewHelper> extends BasePresenter<V>
        implements WeatherPresenterHelper<V>
{
    @Inject
    public WeatherPresenter(DataHelper dataHelper,
            SchedulerProvider schedulerProvider,
            CompositeDisposable compositeDisposable)
    {
        super(dataHelper, schedulerProvider, compositeDisposable);
    }


    @Override
    public void loadForecast(String name, int state)
    {
        if (getDataHelper().getNetworkMode() == AppConstants.NETWORK_MODE)
        {
            getMvpView().showLoading();
            getCompositeDisposable().add(getDataHelper().getForecastApi(name, state)
                    .subscribeOn(getSchedulerProvider().io())
                    .observeOn(getSchedulerProvider().ui())
                    .subscribe(response ->
                    {
                        if (!isViewAttached())
                        {
                            return;
                        }
                        getMvpView().hideLoading();
                        getMvpView().updateForecast(response);
                    }, throwable ->
                    {
                        if (!isViewAttached())
                        {
                            return;
                        }
                        getMvpView().hideLoading();
                        getMvpView().showError("Данные были загружены с ошибкой: " + throwable.getMessage());
                    }));
        } else
        {
            getMvpView().showError("Error");
        }
    }
}
