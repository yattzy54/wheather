package com.medhelp.mamontino.wheather.ui.main;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.medhelp.mamontino.wheather.MainApp;
import com.medhelp.mamontino.wheather.R;
import com.medhelp.mamontino.wheather.data.model.City;
import com.medhelp.mamontino.wheather.data.model.CityLocal;
import com.medhelp.mamontino.wheather.ui.base.BaseActivity;
import com.medhelp.mamontino.wheather.ui.city_fr.CityFragment;
import com.medhelp.mamontino.wheather.ui.pager.PagerActivity;
import com.medhelp.mamontino.wheather.utils.rx.RxEvents;
import com.medhelp.mamontino.wheather.utils.view.ItemListDecorator;
import com.medhelp.mamontino.wheather.utils.view.RecyclerViewClickListener;
import com.medhelp.mamontino.wheather.utils.view.RecyclerViewTouchListener;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

public class MainActivity extends BaseActivity implements MainViewHelper
{
    @Inject
    MainPresenterHelper<MainViewHelper> presenter;

    private CitiesAdapter adapter;

    @Inject
    CompositeDisposable disposables;

    @Inject
    LinearLayoutManager layoutManager;

    @BindView(R.id.rv_main)
    RecyclerView recyclerView;

    @BindView(R.id.tv_err_main)
    TextView tvErrCon;

    @BindView(R.id.toolbar_main)
    Toolbar toolbar;

    private List<CityLocal> cityLocalCash;
    private List<City> cities;

    public static Intent getStartIntent(Context context)
    {
        return new Intent(context, MainActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getActivityComponent().inject(this);
        ButterKnife.bind(this);
        setupToolbar();

        presenter.onAttach(MainActivity.this);

        if (!presenter.isNetworkMode())
        {
            tvErrCon.setVisibility(View.VISIBLE);
        }

        cityLocalCash = new ArrayList<>();
        presenter.getCityLocal();

        setupRxBus();
        setUp();
    }

    private void setupRxBus()
    {
        disposables.add(((MainApp) getApplication())
                .bus()
                .toObservable()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object ->
                {
                    if (object instanceof RxEvents.hasConnection)
                    {
                        tvErrCon.setVisibility(View.GONE);
                    } else if (object instanceof RxEvents.noConnection)
                    {
                        tvErrCon.setVisibility(View.VISIBLE);
                    }
                }));
    }

    private void setupToolbar()
    {
        setSupportActionBar(toolbar);
    }

    @Override
    protected void setUp()
    {
        Timber.d("setUp");

        adapter = new CitiesAdapter(new ArrayList<>());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.addItemDecoration(new ItemListDecorator(this));
        recyclerView.setAdapter(adapter);
        recyclerView.addOnItemTouchListener(new RecyclerViewTouchListener(this, recyclerView, new RecyclerViewClickListener()
        {
            @Override
            public void onClick(View view, int position)
            {
                showPagerActivity();
            }

            @Override
            public void onLongClick(View view, int position)
            {

            }
        }));
    }

    @Override
    protected void onStart()
    {
        super.onStart();

        if (presenter.isNetworkMode())
        {
            Timber.d("Соединение с сетью, getCity from Network");
            presenter.getData(cityLocalCash);
        } else
        {
            Timber.d("Отсутствует соединение с сетью, getCityLocal");

            cities = new ArrayList<>();
            City city;

            if (cityLocalCash.size() > 0)
            {
                for (CityLocal cityLocal : cityLocalCash)
                {
                    city = new City(cityLocal.getName());
                    cities.add(city);
                    Timber.d(city.getName());
                }

            } else
            {
                Timber.d("cityLocalCash == 0");
            }
            adapter.addItems(cities);
        }
    }

    private void showPagerActivity()
    {
        Intent intent = PagerActivity.getStartIntent(this);
        startActivity(intent);
    }

    @Override
    public void openNetworkSettings()
    {
        Intent intent = new Intent(Settings.ACTION_DATA_ROAMING_SETTINGS);
        ComponentName cName = new ComponentName("com.android.phone", "com.android.phone.Settings");
        intent.setComponent(cName);
    }

    @Override
    public void setData(List<City> response)
    {
        Timber.d("setData: " + response.size());

        if (cities != null && cities.size() > 0)
        {
            cities.clear();
            cities.addAll(response);
        }
        adapter.addItems(response);
    }

    @Override
    public void setData(City response)
    {
        Timber.d("setData: " + response.getName());
        cities.add(response);
        adapter.addItems(cities);
    }

    @Override
    public void setCities(List<CityLocal> response)
    {
        Timber.d("setCities");
        cityLocalCash.clear();
        cityLocalCash.addAll(response);
    }

    @Override
    protected void onDestroy()
    {
        presenter.onDetach();
        super.onDestroy();
    }

    @SuppressWarnings("unused")
    @OnClick(R.id.fab_main)
    public void fabClick()
    {
        showCityFragment();
    }

    public void showCityFragment()
    {
        CityFragment.newInstance().show(getSupportFragmentManager());
    }

    @SuppressWarnings("unused")
    public void findCity(String city)
    {
        Timber.d("onSendCityClick: " + city);
        presenter.findCity(city);
    }
}
