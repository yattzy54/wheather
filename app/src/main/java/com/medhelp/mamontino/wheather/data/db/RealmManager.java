package com.medhelp.mamontino.wheather.data.db;


import android.content.Context;

import com.medhelp.mamontino.wheather.data.model.CityLocal;
import com.medhelp.mamontino.wheather.data.model.WeatherLocal;
import com.medhelp.mamontino.wheather.di.scope.PerApplication;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Completable;
import io.reactivex.Single;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import timber.log.Timber;


@PerApplication
public class RealmManager implements RealmHelper
{
    private Context context;

    private RealmConfiguration config = new RealmConfig().getConfig();

    @Inject
    public RealmManager(@PerApplication Context context)
    {
        this.context = context;
    }

    @Override
    public Single<List<WeatherLocal>> getWeatherLocal()
    {
        Realm.init(context);
        Realm realm = Realm.getInstance(config);
        List<WeatherLocal> responses = realm.copyFromRealm(realm
                .where(WeatherLocal.class)
                .findAll());
        realm.close();
        return Single.just(responses);
    }

    @Override
    public Completable saveWeatherLocal(List<WeatherLocal> response)
    {
        try
        {
            return Completable.fromAction(() ->
            {
                Realm.init(context);
                Realm realm = Realm.getInstance(config);
                realm.beginTransaction();
                realm.insertOrUpdate(response);
                realm.commitTransaction();
                realm.close();
                Realm.compactRealm(config);
            });
        } catch (Exception e)
        {
            Timber.e("Сохранение WeatherLocal произошло с ошибкой: " + e.getMessage());
        }
        return Completable.complete();
    }

    @Override
    public Single<List<CityLocal>> getCityLocal()
    {
        Realm.init(context);
        Realm realm = Realm.getInstance(config);
        List<CityLocal> response = realm.copyFromRealm(realm
                .where(CityLocal.class)
                .findAll());
        realm.close();
        return Single.just(response);

    }

    @Override
    public Completable saveCityLocal(List<CityLocal> response)
    {
        try
        {
            return Completable.fromAction(() ->
            {
                Realm.init(context);
                Realm realm = Realm.getInstance(config);
                realm.beginTransaction();
                realm.insertOrUpdate(response);
                realm.commitTransaction();
                realm.close();
                Realm.compactRealm(config);
            });
        } catch (Exception e)
        {
            Timber.e("Сохранение CityLocal произошло с ошибкой: " + e.getMessage());
        }
        return Completable.complete();
    }

    @Override
    public Completable saveCityLocal(CityLocal response)
    {
        try
        {
            return Completable.fromAction(() ->
            {
                Realm.init(context);
                Realm realm = Realm.getInstance(config);
                realm.beginTransaction();
                realm.insertOrUpdate(response);
                realm.commitTransaction();
                realm.close();
                Realm.compactRealm(config);
            });
        } catch (Exception e)
        {
            Timber.e("Сохранение погоды произошло с ошибкой: " + e.getMessage());
        }
        return Completable.complete();
    }
}
