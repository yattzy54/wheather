package com.medhelp.mamontino.wheather.data.network;


import com.medhelp.mamontino.wheather.data.model.City;

import io.reactivex.Observable;

public interface NetworkHelper
{
    Observable<City> getCityApi (String name);

    Observable<City> getForecastApi (String name, int count);
}
